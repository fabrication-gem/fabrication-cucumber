# frozen_string_literal: true

require_relative 'lib/fabrication/cucumber/version'

Gem::Specification.new do |spec|
  spec.name = 'fabrication-cucumber'
  spec.version = Fabrication::Cucumber::VERSION
  spec.authors = ['Paul Elliott']
  spec.email = ['paul@codingfrontier.com']

  spec.summary = 'This gem is an extension for the fabricatgion gem that adds cucumber support.'
  spec.description = 'This gem enables cucumber support for your fabrication environment.'
  spec.homepage = 'http://fabricationgem.org'
  spec.license = 'MIT'
  spec.required_ruby_version = '>= 2.6.0'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/fabrication-gem/fabrication-cucumber'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/fabrication-gem/fabrication-cucumber/-/blob/master/CHANGELOG.md'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.metadata = {
    'rubygems_mfa_required' => 'true'
  }

  # Uncomment to register a new dependency of your gem
  spec.add_dependency 'cucumber', '>= 5.0.0'

  spec.add_development_dependency 'appraisal'
end
