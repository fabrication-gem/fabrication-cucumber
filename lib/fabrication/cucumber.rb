# frozen_string_literal: true

require_relative 'cucumber/version'

module Fabrication
  module Cucumber
    class Error < StandardError; end
    # Your code goes here...
  end
end
