# frozen_string_literal: true

module Fabrication
  module Cucumber
    VERSION = '0.1.0'
  end
end
